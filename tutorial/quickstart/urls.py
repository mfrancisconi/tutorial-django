from django.urls import path  
from quickstart.views import SnippetViewSet

urlpatterns = [
    path('snippets/', SnippetViewSet, name='snippets')
]
